import ballerina/io;

usersClient ep = check new ("http://localhost:9090");

public function main() returns error? {

    io:println("\n1. View User\n2. Create User\n3. Update User\n4. Delete User");
    string opt = io:readln("\n\nEnter option: ");

    match opt {
        "1" => {
            return view();
        }
        "2" => {
            return create();
        }
        "3" => {
            return update();
        }
        "4" => {
            return delete();
        }
    }

}

function view() returns error? {
    io:println("About to send an RPC read request to the server...");
    json readResponse = check ep->view_user({
        userId: "webmagic"
    });

    io:println(readResponse);
}

function create() returns error? {
    io:println("About to send an RPC create request to server");

    json createResponse = check ep->create_user({
        username: "backy",
        lastname: "Andre",
        firstname: "Lionel",
        email: "lionel@gmail.com"
    });

    io:println(createResponse);

}

function update() returns error? {
    io:println("About to send an RPC update request to the server");

    json updateResponse = check ep->update_user({
        userId: "webmagic",
        firstname: "Web"
    });

    io:println(updateResponse);
}

function delete() returns error? {
    io:println("About to send an RPC delete request to the server...");

    string deleteResponse = check ep->delete_user({
        userId: "backy"
    });

    io:println(deleteResponse);
}

