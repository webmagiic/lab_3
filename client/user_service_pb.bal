import ballerina/grpc;

# Description  
public isolated client class usersClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function create_user(CreateRequest|ContextCreateRequest req) returns (CreateResponse|grpc:Error) {
        map<string|string[]> headers = {};
        CreateRequest message;
        if (req is ContextCreateRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/create_user", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <CreateResponse>result;
    }

    isolated remote function create_userContext(CreateRequest|ContextCreateRequest req) returns (ContextCreateResponse|grpc:Error) {
        map<string|string[]> headers = {};
        CreateRequest message;
        if (req is ContextCreateRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/create_user", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <CreateResponse>result, headers: respHeaders};
    }

    isolated remote function view_user(ReadRequest|ContextReadRequest req) returns (ReadResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ReadRequest message;
        if (req is ContextReadRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/view_user", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <ReadResponse>result;
    }

    isolated remote function view_userContext(ReadRequest|ContextReadRequest req) returns (ContextReadResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ReadRequest message;
        if (req is ContextReadRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/view_user", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <ReadResponse>result, headers: respHeaders};
    }

    isolated remote function delete_user(ReadRequest|ContextReadRequest req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        ReadRequest message;
        if (req is ContextReadRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/delete_user", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function delete_userContext(ReadRequest|ContextReadRequest req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        ReadRequest message;
        if (req is ContextReadRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/delete_user", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function update_user(UpdateRequest|ContextUpdateRequest req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        UpdateRequest message;
        if (req is ContextUpdateRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/update_user", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function update_userContext(UpdateRequest|ContextUpdateRequest req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        UpdateRequest message;
        if (req is ContextUpdateRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/update_user", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }
}

# Description  
public client class UsersReadResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendReadResponse(ReadResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextReadResponse(ContextReadResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

# Description  
public client class UsersCreateResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCreateResponse(CreateResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCreateResponse(ContextCreateResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

# Description  
public client class UsersStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

# Description
#
# + content - Field Description  
# + headers - Field Description  
public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

# Description
#
# + content - Field Description  
# + headers - Field Description  
public type ContextReadResponse record {|
    ReadResponse content;
    map<string|string[]> headers;
|};

# Description
#
# + content - Field Description  
# + headers - Field Description  
public type ContextUpdateRequest record {|
    UpdateRequest content;
    map<string|string[]> headers;
|};

# Description
#
# + content - Field Description  
# + headers - Field Description  
public type ContextCreateRequest record {|
    CreateRequest content;
    map<string|string[]> headers;
|};

public type ContextCreateResponse record {|
    CreateResponse content;
    map<string|string[]> headers;
|};

# Description
#
# + content - Field Description  
# + headers - Field Description  
public type ContextReadRequest record {|
    ReadRequest content;
    map<string|string[]> headers;
|};

# Description
#
# + username - Field Description  
# + lastname - Field Description  
# + firstname - Field Description  
# + email - Field Description  
# + userId - Field Description  
public type ReadResponse record {|
    string username = "";
    string lastname = "";
    string firstname = "";
    string email = "";
    string userId = "";
|};

# Description
#
# + lastname - Field Description  
# + firstname - Field Description  
# + email - Field Description  
# + userId - Field Description  
public type UpdateRequest record {|
    string lastname = "";
    string firstname = "";
    string email = "";
    string userId = "";
|};

# Description
#
# + username - Field Description  
# + lastname - Field Description  
# + firstname - Field Description  
# + email - Field Description  
public type CreateRequest record {|
    string username = "";
    string lastname = "";
    string firstname = "";
    string email = "";
|};

# Description
#
# + userId - Field Description  
public type CreateResponse record {|
    string userId = "";
|};

# Description
#
# + userId - Field Description  
public type ReadRequest record {|
    string userId = "";
|};

const string ROOT_DESCRIPTOR = "0A12757365725F736572766963652E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F227B0A0D43726561746552657175657374121A0A08757365726E616D651801200128095208757365726E616D65121A0A086C6173746E616D6518022001280952086C6173746E616D65121C0A0966697273746E616D65180320012809520966697273746E616D6512140A05656D61696C1804200128095205656D61696C22280A0E437265617465526573706F6E736512160A06757365724964180120012809520675736572496422250A0B526561645265717565737412160A0675736572496418012001280952067573657249642292010A0C52656164526573706F6E7365121A0A08757365726E616D651801200128095208757365726E616D65121A0A086C6173746E616D6518022001280952086C6173746E616D65121C0A0966697273746E616D65180320012809520966697273746E616D6512140A05656D61696C1804200128095205656D61696C12160A06757365724964180520012809520675736572496422770A0D55706461746552657175657374121A0A086C6173746E616D6518012001280952086C6173746E616D65121C0A0966697273746E616D65180220012809520966697273746E616D6512140A05656D61696C1803200128095205656D61696C12160A06757365724964180420012809520675736572496432D9010A057573657273122E0A0B6372656174655F75736572120E2E437265617465526571756573741A0F2E437265617465526573706F6E736512280A09766965775F75736572120C2E52656164526571756573741A0D2E52656164526573706F6E736512390A0B64656C6574655F75736572120C2E52656164526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123B0A0B7570646174655F75736572120E2E557064617465526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "user_service.proto": "0A12757365725F736572766963652E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F227B0A0D43726561746552657175657374121A0A08757365726E616D651801200128095208757365726E616D65121A0A086C6173746E616D6518022001280952086C6173746E616D65121C0A0966697273746E616D65180320012809520966697273746E616D6512140A05656D61696C1804200128095205656D61696C22280A0E437265617465526573706F6E736512160A06757365724964180120012809520675736572496422250A0B526561645265717565737412160A0675736572496418012001280952067573657249642292010A0C52656164526573706F6E7365121A0A08757365726E616D651801200128095208757365726E616D65121A0A086C6173746E616D6518022001280952086C6173746E616D65121C0A0966697273746E616D65180320012809520966697273746E616D6512140A05656D61696C1804200128095205656D61696C12160A06757365724964180520012809520675736572496422770A0D55706461746552657175657374121A0A086C6173746E616D6518012001280952086C6173746E616D65121C0A0966697273746E616D65180220012809520966697273746E616D6512140A05656D61696C1803200128095205656D61696C12160A06757365724964180420012809520675736572496432D9010A057573657273122E0A0B6372656174655F75736572120E2E437265617465526571756573741A0F2E437265617465526573706F6E736512280A09766965775F75736572120C2E52656164526571756573741A0D2E52656164526573706F6E736512390A0B64656C6574655F75736572120C2E52656164526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123B0A0B7570646174655F75736572120E2E557064617465526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33"};
}

