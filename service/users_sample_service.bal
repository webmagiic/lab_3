import ballerina/grpc;
import ballerina/io;

type User record {|
    string username;
    string firstname;
    string lastname;
    string email;
    string userId?;
|};

string filePath = "./data/users.json";
json file = check io:fileReadJson(filePath);

map<json> usersData = check file.cloneWithType();
json jsonUsers = usersData.get("users");
User[] users = check jsonUsers.cloneWithType();

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "users" on ep {

    remote function create_user(CreateRequest value) returns CreateResponse|error? {
        io:println("Received an RPC request from client!");

        users.push({username: value.username, firstname: value.firstname, lastname: value.lastname, email: value.email, userId: value.username});
        usersData["users"] = users.toJson();
        check io:fileWriteJson(filePath, usersData);
        return {userId: value.username};
    }
    remote function view_user(ReadRequest value) returns ReadResponse|error {

        ReadResponse user = {};
        foreach int i in 0 ..< users.length() {

            if users[i]?.userId === value.userId {
                user = {...users[i]};
            }
            else{
                return error("User not found");
            }
        }

        io:println("Received an RPC read Request from the client...");
        return user;
    }
    remote function delete_user(ReadRequest value) returns string|error? {

        User[] filteredUsers = users.filter(user => user.get("userId") !== value.userId);

        usersData["users"] = filteredUsers.toJson();

        check io:fileWriteJson(filePath, usersData);

        io:println("Received an RPC delete request from the client...");
        return "User deleted successfully";
    }
    remote function update_user(UpdateRequest value) returns string|error? {
        
        foreach int i in 0 ..< users.length() {

            if users[i].get("userId") === value.userId {
                string checkFirstName;
                string checkLastName;
                string checkEmail;
                
                if value.firstname === ""{ 
                    value.firstname = users[i].firstname;
                }
                if value.email === ""{
                    value.email = users[i].email;
                }
                if value.lastname === ""{
                    value.lastname = users[i].lastname;
                }

                users[i] = {username: users[i].username, firstname: value.firstname, lastname: value.lastname, email: value.email, userId: value.userId};
            }
        }

        usersData["users"] = users.toJson();

        check io:fileWriteJson(filePath, usersData);

        io:println("Received an RPC update request from the client...");
        return "User updated";
    }
}

